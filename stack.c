#include <stdio.h>
#include <stdlib.h>
#include "stack.h"

Stack* create_stack(const int size)
{
    Stack* stack = (Stack*)malloc(sizeof(Stack*));
    stack->top = -1;
    stack->size = size;
    stack->leaves = (Leaf**)malloc(size * sizeof(Leaf*));
    return stack;
}

int full(const Stack* stack)
{
    return stack->top == stack->size -1;
}

int empty(const Stack* stack)
{
    return stack->top == -1;
}

void push(Stack* stack, Leaf* leaf) 
{
    if (full(stack))
        return;

    stack->leaves[++stack->top] = leaf;
}

Leaf* pop(Stack* stack)
{
    if (empty(stack))
        return NULL;
    
    return stack->leaves[stack->top--];
}

Leaf* peek(const Stack* stack)
{
    if (empty(stack))
        return NULL;

    return stack->leaves[stack->top];
}