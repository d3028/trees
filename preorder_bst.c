#include <stdio.h>
#include "stack.h"

#define SIZE 6

Leaf* bst_from_preorder(const int* values) 
{
    Stack* stack = create_stack(SIZE);
    Leaf* root = create_leaf(values[0]);
    push(stack, root);

    Leaf* current;
    for (int i = 1; i < SIZE; i++)
    {
        current = NULL;
        while (!empty(stack) && values[i] > peek(stack)->payload)
        {
            current = pop(stack);
        }

        if (current != NULL)
        {
            current->right_branch = create_leaf(values[i]);
            push(stack, current->right_branch);
        } else {
            peek(stack)->left_branch = create_leaf(values[i]);
            push(stack, peek(stack)->left_branch);
        }
    }

    return root;
}

void print_inorder(const Leaf* leaf)
{
    if (leaf == NULL)
        return;
    
    print_inorder(leaf->left_branch);
    printf("%d->", leaf->payload);
    print_inorder(leaf->right_branch);
}

int main(int argc, char** argv) 
{
    int values[SIZE] = {10, 3, 2, 7, 11, 12};
    Leaf* preorderBst = bst_from_preorder(values);
    print_inorder(preorderBst);
    printf("\n");
    pprint_tree(preorderBst, 3);
    free_tree(preorderBst);
}