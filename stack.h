#include "tree.h"

typedef struct Stack
{
    int top;
    int size;
    Leaf** leaves;
} Stack;

Stack* create_stack(const int size);

int full(const Stack* stack);

int empty(const Stack* stack);

void push(Stack* stack, Leaf* leaf);

Leaf* pop(Stack* stack);

Leaf* peek(const Stack* stack);

