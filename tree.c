#include "tree.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Helper function for creating a Leaf */
Leaf* create_leaf(int payload)
{
    struct Leaf* leaf = (Leaf*)malloc(sizeof(Leaf));
    leaf->payload = payload;
    leaf->left_branch = NULL;
    leaf->right_branch = NULL;
    return leaf;
}

/* Helper function for freeing allocated memory of Leaves */
void free_tree(Leaf* root) 
{
    struct Leaf* current = root;
    if (current->right_branch) {
        free_tree(current->right_branch);
    }
    if (current->left_branch) {
        free_tree(current->left_branch);
    }
    free(current);
}

/* Helper function for printing a tree to the screen */
void print_tree(Leaf* root) 
{
    printf("%d\n", root->payload);
    if (root->left_branch) 
    {
        printf("/\n");
        print_tree(root->left_branch);
    }
    if (root->right_branch) 
    {
        printf("\\\n");
        print_tree(root->right_branch);
    }
}

char* empty_str(const int size, const char c)
{
    char* str = malloc(size + 1);
    memset(str, c, size);
    return str;
}

void print_level(const Leaf* leaf, const int level)
{
    if (leaf == NULL)
        return;
    
    if (level == 1)
    {
        printf("%d ", leaf->payload);
    }
    else 
    {
        print_level(leaf->left_branch, level - 1);
        print_level(leaf->right_branch, level - 1);
    }

}

void pprint_tree(const Leaf* root, const int depth)
{
    for (int i = 1; i <= depth; i++)
    {
        print_level(root, i);
        if (i != depth) 
            printf("\n| %s", empty_str(i, '\\'));
        printf("\n");
    }
}