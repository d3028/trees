/* Leaf Node of Binary Tree */
typedef struct Leaf
{
    int payload;
    struct Leaf* left_branch;
    struct Leaf* right_branch;
} Leaf;

/* Helper function for creating a Leaf */
struct Leaf* create_leaf(int payload);

/* Helper function for freeing allocated memory of Leaves */
void free_tree(struct Leaf* root);

/* Helper function for printing a tree to the screen */
void print_tree(struct Leaf* root);

void pprint_tree(const Leaf* root, const int depth);