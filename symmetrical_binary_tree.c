#include <stdio.h>
#include "tree.h"

int is_symmetrical(Leaf* left, Leaf* right)
{
    if (!left && !right) 
    {
        /* Return true when the bottom of the tree is hit */
        return 1;
    }

    if (left->payload == right->payload)
        /* Recursively search through the sub-trees (comparing the outer and inner sub-trees) */
        return is_symmetrical(left->left_branch, right->right_branch) &&
            is_symmetrical(left->right_branch, right->left_branch);

    /* Return false if the payloads of the leaves are not equal */
    return 0;
}

int main(int argc, char** argv) 
{
    struct Leaf* root = create_leaf(1);

    /* Level 2 */
    root->left_branch = create_leaf(2);
    root->right_branch = create_leaf(2);

    /* Level 3 */
    root->left_branch->left_branch = create_leaf(4);
    root->left_branch->right_branch = create_leaf(5);

    root->right_branch->left_branch = create_leaf(5);
    root->right_branch->right_branch = create_leaf(4);

    printf("Binary Tree is symmetrical: %s\n", 
        is_symmetrical(root, root) ? "True" : "False");

    free_tree(root);

    return 0;
}